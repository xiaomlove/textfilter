<?php
$startTime = microtime(true);
$m = memory_get_peak_usage(true);
echo "start memory use: " . $m/1024/1024 . "MB \n";

$srcPath = __DIR__;

require dirname($srcPath) . '/vendor/autoload.php';

$badwords = file($srcPath . '/resource/keywords', FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
$badwordsCount = count($badwords);
echo "badwords count: " . $badwordsCount . "\n";
echo "load time: " . (microtime(true) - $startTime) . "s \n";
$m = memory_get_peak_usage(true);
echo "load badwords memory use: " . $m/1024/1024 . "MB \n";

$beginBuildTime = microtime(true);
$trie = new TextFilter\Trie();
foreach ($badwords as $bd)
{
	$trie->insertWord($bd);
}
echo "build tree time: " . (microtime(true) - $beginBuildTime) . "s \n";
$m = memory_get_peak_usage(true);
echo "build tree memory use: " . $m/1024/1024 . "MB \n";

/*
$time = microtime(true);
$trie->buildAC();
$m = memory_get_peak_usage(true);
echo "build AC time: " . (microtime(true) - $time) . "s \n";
echo "build AC memory use: " . $m/1024/1024 . "MB \n";
*/



// http服务绑定的ip及端口
$serv = new swoole_http_server("127.0.0.1", 9502);

echo "server start at 127.0.0.1:9502 \n";


$GLOBALS['trie'] = $trie;
$GLOBALS['badwords_count'] = $badwordsCount;


/**
 * 处理请求
 */
$serv->on('Request', function($request, $response) {
    if (strtoupper($request->server['request_method']) !== 'POST')
    {
        $result = file_get_contents(__DIR__ . '/public/index.html');
        $response->header("X-Server", php_uname());
        $response->header('Content-Type', 'Content-Type: text/html; charset=utf-8');
        return $response->end($result);
    }


	$requestTime = microtime(true);
    // 接收post请求参数
    $content = isset($request->post['content']) ? $request->post['content']: '';

    //搜索类型。normal or ac
    $searchType = isset($request->post['search_type']) ? $request->post['search_type']: '';
    if (!in_array($searchType, ['ac', 'normal']))
    {
        $searchType = 'normal';
    }

    $badwordsCount = $GLOBALS['badwords_count'];
    $trie = $GLOBALS['trie'];

    $result = [
    	'request_time' => $requestTime,
        'content_length' => mb_strlen($content, 'utf8'),
	    'dict_badwords_count' => $badwordsCount,
        'search_type' => $searchType,
    	'badwords_found' => [],
    ];

    if (!empty($content)) 
    {
        if ($searchType == 'ac')
        {
            //ac暂无
        }
        else
        {
            $result['badwords_found'] = $trie->search($content, true);
        }
    }
    $result['total_use_time'] = sprintf(
        '%f ms', 
        number_format((microtime(true) - $requestTime) * 1000, 4)
    );
    $result['memory_use'] = (memory_get_peak_usage(true) / 1024 / 1024) . "MB";
    // 定义http服务信息及响应处理结果
    $response->header("X-Server", php_uname());
    $response->header('Content-Type', 'Content-Type: text/json; charset=utf-8');
    $response->end(json_encode($result));
});

echo "end memory use: " . memory_get_usage(true) / 1024 / 1024 . "MB \n";
$serv->start();

