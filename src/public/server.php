<?php
$startTime = microtime(true);
define('SRC_PATH', dirname(__DIR__));
define('ROOT_PATH', dirname(SRC_PATH));
require ROOT_PATH . '/vendor/autoload.php';

$return = [
    'content_length' => '',
    'badwords_count' => 18967,
    'search_type' => '',
    'search_time_use' => '',
    'curl_time_use' => '',
    'total_time_use' => '',
    'memory_use' => '',
    'errstr' => '',
    'badwords_found' => [],
];

function outputJson()
{
    global $return;
    die(json_encode($return));
}
function formatTimeUse($timeuse)
{
    return number_format($timeuse * 1000, 2) . "ms";
}

function getFromDaemon($post)
{
    if (stripos($post['search_type'], 'swoole') !== false)
    {
        $url = 'http://127.0.0.1:9502';//swoole
    }
    else 
    {
        $url = 'http://127.0.0.1:9503';//workerman
    }
    $t1 = microtime(true);
    $ch = curl_init($url);
    curl_setopt_array($ch, [
        CURLOPT_CONNECTTIMEOUT_MS => 200,
        CURLOPT_TIMEOUT_MS => 1500,
        CURLOPT_HEADER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,//不输出
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($post),
    ]);
    $result = curl_exec($ch);
    if ($result === false)
    {
        return ['err' => curl_error($ch), 'data' => ''];
    }
    else
    {
        $result = json_decode($result, true);//返回的是json字符串
        $result['curl_time_use'] = formatTimeUse(microtime(true) - $t1);
        return ['err' => null, 'data' => $result];
    }
}

if (empty($_POST['content']))
{
    outputJson();
}
//内容
$content = trim($_POST['content']);
$return['content_length'] = mb_strlen($content, 'utf8');

//搜索方式
$searchTypeArr = [
    'redis_normal', 
    'serialized_normal', 
    'daemon_swoole_normal', 'daemon_swoole_ac', 
    'daemon_workerman_normal', 'daemon_workerman_ac'
];
$searchType = empty($_POST['search_type']) ? 'redis_normal' : $_POST['search_type'];
if (!in_array($searchType, $searchTypeArr))
{
    $searchType = 'redis_normal';
}
$return['search_type'] = $searchType;

$result = [];
switch ($searchType)
{
    case 'redis_normal':
        $trie = new TextFilter\RedisTrie();
        $time = microtime(true);
        $result = $trie->search($content, true);
        
        $return['search_time_use'] = formatTimeUse(microtime(true) - $time);
        $return['badwords_found'] = $result;
        $return['memory_use'] = (memory_get_peak_usage(true) / 1024 / 1024) . "MB";
        break;
    case 'serialized_normal':
        $keywordsStr = file_get_contents(SRC_PATH . '/resource/keyword-file-serialized');
        $keywords = unserialize($keywordsStr);
        $trie = new TextFilter\Trie();
        $trie->load($keywords);
        $time = microtime(true);
        $result = $trie->search($content, true);
        
        $return['search_time_use'] = formatTimeUse(microtime(true) - $time);
        $return['badwords_found'] = $result;
        $return['memory_use'] = (memory_get_peak_usage(true) / 1024 / 1024) . "MB";
        break;
    case 'daemon_swoole_normal':
    case 'daemon_swoole_ac':
    case 'daemon_workerman_normal':
    case 'daemon_workerman_ac':
        $time = microtime(true);
        $daemonResult = getFromDaemon(['search_type' => $searchType, 'content' => $content]);
        if (!is_null($daemonResult['err']))
        {
            $return['errstr'] = $daemonResult['err'];
        }
        else
        {
            $return['curl_time_use'] = $daemonResult['data']['curl_time_use'];
            $return['search_time_use'] = $daemonResult['data']['total_use_time'];
            $return['badwords_found'] = $daemonResult['data']['badwords_found'];
            $return['badwords_count'] = $daemonResult['data']['dict_badwords_count'];
            $return['memory_use'] = $daemonResult['data']['memory_use'];
        }
        break;
    default:
        $time = microtime(true);
        $return['errstr'] = 'error search_type';
        break;
}

$return['total_time_use'] = formatTimeUse(microtime(true) - $startTime);

outputJson();









