<?php
namespace TextFilter;

class Base
{
    /**
     * 获取字符数组
     * @param string $str
     * @return array
     */
    protected static function getCharacterArray($str)
    {
        return preg_split('//u', trim($str), -1, PREG_SPLIT_NO_EMPTY);
    }
}