<?php
namespace TextFilter;

class DoubleArrayTrie extends Base implements FilterInterface
{
    private $base = [];
    
    private $check = [];
    
    public function __construct()
    {
        
    }
    
    public function insertWord($word)
    {
        $characterArr = self::getCharacterArray($word);
        $length = count($characterArr);
        $i = 0;
        $previousWord = '';
        $previousX = 0;
        while ($i < $length)
        {
            $ch = $characterArr[$i];
            $value = $this->getUnicodeValue($ch);
            $x = $previousX + $value;
            $_word = $previousWord . $ch;
            if (!isset($this->base[$x]))
            {
                $node = $this->createNode($ch);
                $node['word'] = $_word;
                if ($i == $length - 1)
                {
                    $node['is_end'] = 1;
                }
                $this->base[$x] = $node;
                $this->check[$x] = $previousX;
            }
            $previousWord = $_word;
            $previousX = $x;
            $i++;
        }
    }
    
    public function isWordExists($word)
    {
        $characterArr = self::getCharacterArray($word);
        $length = count($characterArr);
        $i = 0;
        $previous = 0;
        while ($i < $length)
        {
            $ch = $characterArr[$i];
            $value = $this->getUnicodeValue($ch);
            $x = $previous + $value;
            if (!isset($this->base[$x]))
            {
                echo "false 1 <br/>";
                return false;
            }
            if ($i === $length - 1)
            {
                $node = $this->base[$x];
                if (!$node['is_end'])
                {
                    echo "false 2 <br/>";
                    return false;
                }
            }
            
            $i++;
            $previous = $x;
        }
        return true;
    }
    
    public function deleteWord($word)
    {
        
    }
    
    public function search($string, $fetchAll = false)
    {
        
    }
    
    public function dumpBase()
    {
        echo '<pre>';
        var_dump($this->base);
        echo '</pre>';
    }
    
    public function dumpCheck()
    {
        echo '<pre>';
        var_dump($this->check);
        echo '</pre>';
    }
    
    private function getUnicodeValue($character)
    {
        $ord = ord($character);
        if ($ord >=0 && $ord <= 127)
        {
            return $ord;
        }
        elseif ($ord > 127)
        {
            $result = unpack('H6', $character);
            return hexdec($result[1]);
        }
        else
        {
            return false;
        }
    }
    
    private function createNode($ch)
    {
        return [
            'character' => $ch,
            'word' => '',
            'is_end' => 0,
        ];
    }
}