<?php
namespace TextFilter;

class RedisTrie extends Base implements FilterInterface
{
    private $redis;
    
    private $prefix = 'trie:';
    
    public function __construct($host = '127.0.0.1', $port = 6379)
    {
        $redis = new \Redis();
        $redis->connect($host, $port);
        $this->redis = $redis;
    }
    
    /**
     * 插入一个词
     * @param string $word 一个词，当包含"&"时表示要同时出现这多个词才能被搜索到
     * @param array 这个词的额外相关信息，关联数组。如filter_words_id，表示对应屏蔽规则ID
     * @return void
     * 
     */
    public function insertWord($word, array $extra = [])
    {
        $word = trim($word);
        if (empty($word))
        {
            return null;
        }
        $extra = !empty($extra) ? json_encode((array)$extra) : '';
        if (stripos($word, '&') === false)
        {
            //没有&，单纯的一个词
            $this->insertPureWord($word);
            $attr['is_end'] = 1;//必须is_end
            $attr['extra'] = $extra;
            $this->setNodeAttr($word, $attr);
        }
        else
        {
            $words = explode('&', $word);
            $wordsFlip = array_flip($words);
            foreach ($words as $_word)
            {
                if (empty($_word))
                {
                    continue;
                }
                $this->insertPureWord($_word);
    
                $andWords = $wordsFlip;
                unset($andWords[$_word]);//去掉自己
                $attr = [];
                $attr['and_words'] = implode(',', array_keys($andWords));
    
                $attr['extra'] = $extra;
                $this->setNodeAttr($_word, $attr);
            }
        }
    }
    
    /**
     * 插入一个单纯的词，不含“&”。
     * 逐个字符遍历，每个字符为trie树的一个节点，其键为从根到这个节点的路径（含自己），值是这个词的信息，hash保存。
     * @param string $word 词
     * @return void
     */
    private function insertPureWord($word)
    {
        $characterArr = self::getCharacterArray($word);
        if (empty($characterArr))
        {
            return false;
        }
        $previous = '';
        foreach ($characterArr as $index => $ch)
        {
            $path = $previous . $ch;
            //无论第几层，若没有，都要插入
            if (!$this->hasNode($path))
            {
                $node = $this->createNode($ch);
                if ($previous === '')
                {
                    //若是第一层，标记root
                    $node['is_root'] = 1;
                }
                $node['word'] = $path;
                $this->insertNode($path, $node);
            }
            $previous .= $ch;
        }
        return true;
    }
    
    public function flushFromMysql()
    {
        
    }
    
    /**
     * 删除一个词
     * @param string $word
     * @return void
     */
    public function deleteWord($word)
    {
        if (stripos($word, '&') === false)
        {
            //没有&，将is_end标记为0即不会被搜索到
            if ($this->hasNode($word))
            {
                $this->setNodeAttr($word, ['is_end' => 0]);
            }
        }
        else
        {
            //有&，将每个的and_words清空即不会被搜索到
            $wordArr = explode('&', $word);
            foreach ($wordArr as $_word)
            {
                if (!empty($_word) && $this->hasNode($_word))
                {
                    $this->setNodeAttr($_word, ['and_words' => '']);
                }
            }
        }
    }
    
    /**
     * 获取一个词的信息
     * @param string $word
     * @return false|array
     */
    public function getWord($word)
    {
        if (stripos($word, '&') === false)
        {
            return $this->getNode($word);
        }
        else 
        {
            $out = [];
            $wordArr = explode('&', $word);
            foreach ($wordArr as $_word)
            {
                if (!empty($_word))
                {
                    $out[] = $this->getNode($_word);
                }
            }
            return $out;
        }
    }
    
    /**
     * 从一段文本中搜索包含的词
     * @param string $string 文本
     * @param boolean $fetchAll 是否搜索所有包含的词，默认false，搜索到一个就返回。
     * @return array
     */
    public function search($string, $fetchAll = false)
    {
        $characterArr = self::getCharacterArray($string);
        if (empty($characterArr))
        {
            return [];
        }
        $out = [];
        $length = count($characterArr);
        foreach ($characterArr as $i => $ch)
        {
            $j = $i;
            $previous = '';
            while ($j < $length)
            {
                $childCh = $characterArr[$j];
                $path = $previous . $childCh;
    
                if (!$this->hasNode($path))
                {
                    break;//根，第一层
                }
                $node = $this->getNode($path);
                //如果标记为is_end，这是一个单独的词，被匹配
                if ($node['is_end'] === '1')
                {
                    //echo "ch: $childCh, 1得到: {$node['word']}, i: $i, j: $j <br/>";
    
                    $out[] = $node;
                    if (!$fetchAll)
                    {
                        return $out;
                    }
                }
                //不标记为is_end，得有and_words跟其他词共同出现，也能匹配
                if (!empty($node['and_words']))
                {
                    //有“且”的词，必须同时出现“且”的所有词才算
                    $andWordsArr = explode(',', $node['and_words']);
                    $matchAll = true;
                    foreach ($andWordsArr as $and_word)
                    {
                        if (stripos($string, $and_word) === false)
                        {
                            $matchAll = false;
                            break;
                        }
                    }
                    if ($matchAll)
                    {
                        $out[] = $node;
                        if (!$fetchAll)
                        {
                            return $out;
                        }
                    }
                }
    
                $previous .= $childCh;
                $j++;
            }
        }
        return $out;
    }
    
    
    /**
     * 获取一个字符对应的节点的键
     * @param string $ch
     * @return string
     */
    private function getNodeKey($ch)
    {
        return $this->prefix . $ch;
    }
    
    /**
     * 判断一个字符是否存在（根下）
     * @param string $ch 字符，即路径，在redis中的键
     * @return boolean
     */
    private function hasNode($ch)
    {
        return $this->redis->exists($this->getNodeKey($ch));
    }
    
    /**
     * 创建一个节点
     * @param string $ch 节点对应的字符
     * @return array
     */
    private function createNode($ch)
    {
        return [
            'is_end' => 0,
            'is_root' => 0,
            'character' => $ch,
            'word' => $ch,
            'and_words' => '',//对应“且”关系的词，多个逗号分割。
            'extra' => '',//额外信息，json串
        ];
    }
    
    
    /**
     * 添加一个节点，是一个hash
     * @param string $ch
     * @return void
     */
    private function insertNode($ch, array $nodeAttr)
    {
        $key = $this->getNodeKey($ch);
    
        foreach ($nodeAttr as $attrKey => $attrValue)
        {
            $this->redis->hSet($key, $attrKey, $attrValue);
        }
    }
    
    /**
     * 设置一个节点的信息
     * @param string $ch
     * @param array $attrValue
     * @return void
     */
    private function setNodeAttr($ch, array $attrValue)
    {
        $key = $this->getNodeKey($ch);
        foreach ($attrValue as $attr => $value)
        {
            $this->redis->hSet($key, $attr, $value);
        }
    }
    
    /**
     * 获取一个节点的信息
     * @param string $ch
     * @param array $attr
     * @return array
     * 
     */
    private function getNode($ch, $attr = null)
    {
        $key = $this->getNodeKey($ch);
        if (is_null($attr))
        {
            return $this->redis->hGetAll($key);
        }
        else
        {
            return $this->redis->hGet($key, $attr);
        }
    }
    
    /**
     * 字符串转化数组
     * @param string $str
     * @return array
     */
    protected static function getCharacterArray($str)
    {
        return preg_split('//u', trim($str), -1, PREG_SPLIT_NO_EMPTY);
    }
    
    /**
     * 打印所有节点信息
     * @return void
     */
    public function dump()
    {
        $redis = $this->redis;
        $keys = $redis->keys($this->prefix . '*');
        var_dump($keys);die;
    
        echo '<pre>';
        foreach ($keys as $key)
        {
            //节点信息, hash
            $title = "节点!!!key: $key<br/>";
            $r = $redis->hGetAll($key);
            echo $title;
            var_dump($r);
            echo '<hr/>';
        }
        echo '</pre>';
    }
}