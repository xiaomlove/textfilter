<?php
namespace TextFilter;

class TrieNode
{
    public $isEnd = false;

    public $character = null;

    public $word = null;

    public $goto = null;

    public $children = [];
    

    public function __construct($character)
    {
        $this->character = $character;
    }

}

class TrieObject
{
    private static $root;
    
    private static $arrayData;
    
    private $redis_host = '127.0.0.1';
    
    private $redis_port = 6379;
    
    private $use_redis = true;
    
    private $redis = null;

    public function __construct()
    {
        $root = new TrieNode(null);
        $root->goto = $root;
        self::$root = $root;
        if ($this->use_redis)
        {
            $this->redis = $this->getRedis();
        }
    }
    
    private function getRedis()
    {
        $redis = new \Redis();
        $redis->connect($this->redis_host, $this->redis_port);
        return $redis;
    }

    public function insertWord($str)
    {
        $current = &self::$root;
        $chArr = $this->getCharacterArr($str);
        foreach ($chArr as $ch)
        {
            if (!isset($current->children[$ch]))
            {
                $node = new TrieNode($ch);
                $node->word = $current->word . $ch;
                $current->children[$ch] = $node;
            }
            $current = &$current->children[$ch];
        }
        $current->isEnd = true;
    }

    public function buildAC()
    {
        $root = &self::$root;
        $arr = [$root];
        while (!empty($arr))
        {
            $current = array_shift($arr);
            foreach ($current->children as $key => &$value)
            {
                if ($value->goto === null)
                {
                    if ($current->character === null)
                    {
                        //第一层的goto是root
                        //echo "第一层，指定为root, character: " . $value->character . "<br/> \n";
                        $value->goto = $root;
                    }
                    else
                    {
                        //沿着父元素的goto找，直到找到一个元素的子孙也含当前元素的字符。
                        //若到root还找不到，则goto为root，否则为那个子元素
                        $ch = $value->character;//当前字符
                        $parentGotoNode = $current->goto;//父元素的goto节点
                        while (true)
                        {
                            if ($parentGotoNode->character === null && !isset($parentGotoNode->children[$ch]))
                            {
                                //echo "非第一层，到了root也没有找到：{$ch} <br/> \n";
                                $value->goto = $root;
                                break;
                            }
                            if (isset($parentGotoNode->children[$ch]))
                            {
                                //echo "非第一层，有找到：{$ch} <br/> \n";
                                $value->goto = $parentGotoNode->children[$ch];
                                break;
                            }
                            $parentGotoNode = $parentGotoNode->goto;
                        }

                    }
                }
                $arr[] = $value;
            }

        }
    }

    public function search($str)
    {
        $current = self::$root;
        $chArr = $this->getCharacterArr($str);
        $len = count($chArr);
        $out = [];
        for ($i = 0; $i < $len; $i++)
        {
            $ch = $chArr[$i];
            if (!isset($current->children[$ch]))//看根下有没有
            {
                continue;
            }

            $startNode = $current->children[$ch];
            if ($startNode->isEnd)
            {
                $out[] = $startNode->word;
            }
            $j = $i + 1;
            while ($j < $len)
            {
                $ch = $chArr[$j];
                if (!isset($startNode->children[$ch]))
                {
                    break;
                }
                $startNode = $startNode->children[$ch];
                if ($startNode->isEnd)
                {
                    $out[] = $startNode->word;
                }
                $j++;
            }
        }
        return $out;
    }
    
    public function searchAC($str)
    {
        $current = self::$root;
        $chArr = $this->getCharacterArr($str);
        $len = count($chArr);
        $out = [];
        for ($i = 0; $i < $len; $i++)
        {
            $ch = $chArr[$i];
            if (isset($current->children[$ch]))
            {
                //当下已经找到，current正常向前了，不再跳
                $current = $current->children[$ch];
                //echo "current: {$current->character} <br/>";
                if ($current->isEnd)
                {
                    $out[] = $current->word;
                }
                $goto = $current->goto;
                while (true)
                {
                    if ($goto->character === null)
                    {
                        break;
                    }
                    if ($goto->isEnd)
                    {
                        $out[] = $goto->word;
                    }
                    $goto = $goto->goto;
                }
            }
            elseif ($current->character !== null)
            {
                //当下没找到，又不是在root，回到root继续尝试
                $current = self::$root;
                if (isset($current->children[$ch]))
                {
                    //当下已经找到，current正常向前了，不再跳
                    $current = $current->children[$ch];
                    //echo "current: {$current->character} <br/>";
                    if ($current->isEnd)
                    {
                        $out[] = $current->word;
                    }
                    $goto = $current->goto;
                    while (true)
                    {
                        if ($goto->character === null)
                        {
                            break;
                        }
                        if ($goto->isEnd)
                        {
                            $out[] = $goto->word;
                        }
                        $goto = $goto->goto;
                    }
                }
            }
        }
        return $out;
    }

    public function delete($str)
    {
        $root = self::$root;
        return $this->doDelete($root, $str, 0, mb_strlen($str, 'utf8'));
    }

    private function doDelete(&$trieNode, $str, $index, $len)
    {
        $ch = mb_substr($str, $index, 1);//字符，也即键
        if (!isset($trieNode->children[$ch]))
        {
            return true;//压根不存在
        }
        if ($index == $len - 1)
        {
            //到所删字符末端。这时trieNode还是它的父节点
            if ($trieNode->children[$ch]->isEnd)
            {
                $trieNode->children[$ch]->isEnd = false;//改变之
            }
            return true;
        }
        return $this->doDelete($trieNode->children[$ch], $str, $index + 1, $len);
    }

    public function dump()
    {
        var_dump(self::$root);
    }

    private function getCharacterArr($str)
    {
        return preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
    }

    public function saveToFile($filename = '')
    {
        $root = &self::$root;
        $arr = [$root];
        $out = [];
        while (!empty($arr))
        {
            $current = array_shift($arr);
            foreach ($current->children as $key => $value)
            {
                //var_dump($current->goto);
                $out[$value->word] = [
                    'goto' => $value->goto->word,
                    'character' => $value->character,
                    'word' => $value->word,
                    'is_end' => $value->isEnd,
                ];
                $arr[] = $value;
            }
        }
        file_put_contents($filename, "<?php\n" . '$keywords = ' . var_export($out, true));
// 	    file_put_contents($filename, serialize($out));
        return $out;
    }

    public function saveToRedis()
    {
        $startTime = microtime(true);
        $redis = $this->redis;
        $root = &self::$root;
        $arr = [$root];
        $out = [];
        while (!empty($arr))
        {
            $current = array_shift($arr);
            foreach ($current->children as $key => $value)
            {
                $hkey =  $this->getRedisKey($value->word);
                $hvalue = [
                    'goto' => (string)$value->goto->word,
                    'character' => (string)$value->character,
                    'word' => (string)$value->word,
                    'is_end' => (int)$value->isEnd,
                ];
                foreach ($hvalue as $_key => $_value)
                {
                    $redis->hSet($hkey, $_key, $_value);
                }
                $arr[] = $value;
            }
        }
        echo __FUNCTION__ . " cost time: " . (microtime(true) - $startTime) . "s</br>";
    }

    public function setArrayData($arrayData)
    {
        self::$arrayData = $arrayData;
    }

    public function searchFromArray($string)
    {
        $chArr = $this->getCharacterArr($string);
        $len = count($chArr);
        $out = [];
        $word = '';
        for ($i = 0; $i < $len; $i++)
        {
            $ch = $chArr[$i];
            if (!isset(self::$arrayData[$ch]))//看根下有没有
            {
                continue;
            }

            $startNode = self::$arrayData[$ch];
            if ($startNode['is_end'])
            {
                $out[] = $ch;
            }
            $j = $i + 1;
            while ($j < $len)
            {
                $ch .= $chArr[$j];
                if (!isset(self::$arrayData[$ch]))
                {
                    break;
                }
                $startNode = self::$arrayData[$ch];
                if ($startNode['is_end'])
                {
                    $out[] = $ch;
                }
                $j++;
            }
        }
        return $out;
    }

    public function searchFromArrayAC($string)
    {
        $chArr = $this->getCharacterArr($string);
        $len = count($chArr);
        $out = [];
        $previous = '';
        $current = self::$arrayData;
        $j = 0;
        for ($i = 0; $i < $len; $i++)
        {
            $j++;
            $ch = $chArr[$i];
            if (isset(self::$arrayData[$previous . $ch]))
            {
                $key = $previous . $ch;
                //echo "有key1: $key <br/>";
            }
            elseif (isset($current['goto']) && isset(self::$arrayData[$current['goto'] . $ch]))
            {
                $key = $current['goto'] . $ch;
                //echo "有key2: $key <br/>";
            }
            elseif (isset(self::$arrayData[$ch]))
            {
                $key = $ch;
                //echo "有key3: $key <br/>";
            }
            else
            {
                continue;
            }

            $current = self::$arrayData[$key];
            $previous = $key;

            if (!empty($current['is_end']))
            {
                //echo "中{$current['word']}--1--key: $key <br/>";
                $out[] = $current['word'];
            }
            if (empty($current['goto']))
            {
                continue;
            }

            $gotoKey = $current['goto'];
            while (!empty($gotoKey) && isset(self::$arrayData[$gotoKey]))
            {
                $j++;
                $goto = self::$arrayData[$gotoKey];
                if ($goto['is_end'])
                {
                    //echo "中{$goto['word']}--2--key: $gotoKey <br/>";
                    $out[] = $goto['word'];
                }
                $gotoKey = $goto['goto'];
            }
        }
        echo "j = $j <br/>";
        return $out;
    }

    private function getRedisKey($ch)
    {
        return 'trie:' . $ch;
    }

    private function isInRedis($ch)
    {
        return $this->redis->exists($this->getRedisKey($ch));
    }

    private function getNodeFromRedis($ch)
    {
        return $this->redis->hGetAll($this->getRedisKey($ch));
    }

    public function searchFromRedisAC($string)
    {
        $redis = $this->redis;
        $chArr = $this->getCharacterArr($string);
        $len = count($chArr);
        $out = [];
        $previous = '';
        $current = [];
        $j = 0;
        $k = 0;
        for ($i = 0; $i < $len; $i++)
        {
            $j++;
            $ch = $chArr[$i];
            if ($this->isInRedis($previous . $ch))
            {
                $k++;
                $key = $previous . $ch;
//                echo "有key1: $key <br/>";
            }
            elseif (!empty($current['goto']) && ($this->isInRedis($current['goto'] . $ch)))
            {
//                echo "goto: " . $current['goto'] . "<br/>";
                $k++;
                $key = $current['goto'] . $ch;
//                echo "有key2: $key <br/>";
            }
            elseif ($this->isInRedis($ch))
            {
                $k++;
                $key = $ch;
//                echo "有key3: $key <br/>";
            }
            else
            {
//                echo "no key $ch <br/>";
                continue;
            }

            $current = $this->getNodeFromRedis($key);
            $k++;
            $previous = $key;

            if (!empty($current['is_end']))
            {
//                echo "中{$current['word']}--1--key: $key <br/>";
                $out[] = $current['word'];
            }
            if (empty($current['goto']))
            {
                continue;
            }

            $gotoKey = $current['goto'];
            while (!empty($gotoKey) && $this->isInRedis($gotoKey))
            {
                $j++;
                $k++;
                $goto = $this->getNodeFromRedis($gotoKey);
                $k++;
                if ($goto['is_end'])
                {
//                    echo "中{$goto['word']}--2--key: $gotoKey <br/>";
                    $out[] = $goto['word'];
                }
                $gotoKey = $goto['goto'];
             }
        }
        echo __FUNCTION__ . " j = $j <br/>";
        echo __FUNCTION__ . " k = $k <br/>";
        return $out;
    }
}