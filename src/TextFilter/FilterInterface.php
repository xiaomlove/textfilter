<?php
namespace TextFilter;

interface FilterInterface
{
    public function insertWord($word);
    
    public function deleteWord($word);
    
    public function search($string, $fetchAll = false);
}