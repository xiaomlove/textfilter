<?php
namespace TextFilter;

class Trie extends Base implements FilterInterface
{
    private $root;
    
    private $unicodeKey = false;
    
    public function __construct($unicodeKey = false)
    {
        $this->root = self::getNode();
        if (is_bool($unicodeKey))
        {
            $this->unicodeKey = $unicodeKey;
        }
    }
    
    /**
     * 插入一个词
     * @param string $word
     */
    public function insertWord($word)
    {
        $characterArr = self::getCharacterArray($word);
        if (empty($characterArr))
        {
            return false;
        }
        $current = &$this->root;
        foreach ($characterArr as $ch)
        {
            if (!isset($current['children'][$ch]))
            {
                $node = self::getNode();
                $node['word'] = $current['word'] . $ch;
                $node['character'] = $ch;
                $current['children'][$ch] = $node;
            }
            $current = &$current['children'][$ch];
        }
        $current['is_end'] = 1;
        return true;
    }
    
    public function buildAC()
    {
        $root = &$this->root;
        $arr = [$root];
        while (!empty($arr))
        {
            $current = array_shift($arr);
            foreach ($current['children'] as $key => &$value)
            {
                if (empty($value['children']))
                {
                    //第一层
                    $value['goto'] = &$root;
                }
                else 
                {
                    $parentGotoNode = &$current['goto'];//父节点的goto
                    while (true)
                    {
                        if ($parentGotoNode['character'] === null && !isset($parentGotoNode['children'][$key]))
                        {
                            //到了root也找不到，goto为root
                            $value['goto'] = &$root;
                            break;
                        }
                        if (isset($parentGotoNode['children'][$key]))
                        {
                            //找到即可，即使是root下的。goto就是找到的那个节点的子节点
                            $value['goto'] = &$parentGotoNode['children'][$key];
                            break;
                        }
                        $parentGotoNode = &$parentGotoNode['goto'];
                    }
                }
                $arr[] = $value;
            }
        }
    }
    
    /**
     * 删除一个词
     * @param string $word
     */
    public function deleteWord($word)
    {
        return self::doDelete($this->root, $word, 0, mb_strlen($word, 'utf8'));
    }
    
    private static function doDelete(&$trieNode, $str, $index, $len)
	{
		$ch = mb_substr($str, $index, 1);//字符，也即键
		if (!isset($trieNode['children'][$ch]))
		{
			return true;//压根不存在
		}
		if ($index == $len - 1)
		{
			//到所删字符末端。这时trieNode还是它的父节点
			if ($trieNode['children'][$ch]['is_end'])
			{
				$trieNode['children'][$ch]['is_end'] = 0;//改变之
			}
			return true;
		}
		return self::doDelete($trieNode['children'][$ch], $str, $index + 1, $len);
	}
    
    public function search($string, $returnWords = false)
    {
        $characterArr = self::getCharacterArray($string);
        if (empty($characterArr))
        {
            return [];
        }
        $out = [];
        $root = &$this->root;
        $length = count($characterArr);
        foreach ($characterArr as $i => $ch)
        {
            if (!isset($root['children'][$ch]))
            {
                continue;
            }
            $current = &$root['children'][$ch];
            if ($current['is_end'])
            {
                $out[] = $returnWords ? $ch : [$i, 1];
            }
            $j = $i + 1;
            while ($j < $length)
            {
                $nextCh = $characterArr[$j];
                if (!isset($current['children'][$nextCh]))
                {
                    break;
                }
                $current = &$current['children'][$nextCh];
                if ($current['is_end'])
                {
                    $out[] = $returnWords ? mb_substr($string, $i, $j - $i + 1, 'utf8') : [$i, $j - $i + 1];
                }
                $j++;
            }
        }
        return $out;
    }
    
    public function searchAC($string)
    {
        $characterArr = self::getCharacterArray($string);
        if (empty($characterArr))
        {
            return [];
        }
        $out = [];
        $root = &$this->root;
        foreach ($characterArr as $i => $ch)
        {
            if (!isset($root['children'][$ch]))
            {
                continue;
            }
            $current = &$root['children'][$ch];
            if ($current['is_end'])
            {
                //第一个就中
                $out[] = $ch;
            }
            //沿着goto走
            $goto = $current['goto'];
            while (true)
            {
                if ($goto['character'] === null)
                {
                    //走到root结束搜索
                    break;
                }
                if (isset($goto['children'][$ch]))
                {
                    //goto中
                    $out[] = $goto['word'];
                }
                $goto = $goto['goto'];
            }
        }
        return $out;
    }
    
    public function dump()
    {
        var_dump($this->root);
    }
    
    public function saveToFile($filename, $serialize = false)
    {
        if ($serialize)
        {
            file_put_contents($filename, serialize($this->root));
        }
        else
        {
            ob_start();
            $arr = var_export($this->root);
            $result = "<?php\nreturn " . ob_get_clean() . ';';
            file_put_contents($filename, $result);
        }
    }
    
    public function load(array $data)
    {
        $this->root = $data;
    }
    
    private static function getNode()
    {
        return [
            'character' => null,
            'is_end' => 0,
            'word' => '',
            'children' => [],
            'goto' => null,
        ];
    }
}